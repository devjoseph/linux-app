from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.models import User

from .models import (GoalStatus, ScrumyGoals, ScrumyHistory)
import random
# Create your views here.

# def get_grading_parameters(request):
#     return HttpResponse('Welcome to Django')

def index(request):
    # return HttpResponse('This is a Scrum Application')
    goal = ScrumyGoals.objects.filter(goal_name='Learn Django')
    return HttpResponse(goal)


def move_goal(request, goal_id):
    # ScrumyGoals.objects.get(goal_id=goal_id)
    # goal = ScrumyGoals.objects.get(goal_name='Learn Django')
    # return HttpResponse(goal)
    try: 
        obj = ScrumyGoals.objects.get(goal_id=goal_id) 
    except Exception as e: 
        return render(
            request, 
            'josephscrumy/exception.html', 
            {'error': 'A record with that goal id does not exist'}
        )
    else: 
        return HttpResponse(obj.goal_name)


def add_goal(request):
    ScrumyGoals.objects.create(
        user=User.objects.get(username='LouisOma'),
        goal_name='Keep Learning Django',
        goal_id= random.randint(1000,9999),
        created_by = 'Louis',
        moved_by='Louis',
        owner='Louis',
        goal_status=GoalStatus.objects.get(status_name='Weekly Goal')
    )


# def home(request):
#     goal_name = ScrumyGoals.objects.get(goal_name='Learn Django')
#     goal_id = int(ScrumyGoals.objects.get(goal_id=1))
#     user = User.objects.get(username='louis')
#     context = {
#         'goal_name': goal_name,
#         'goal_id': goal_id,
#         'user': user,
#     }
#     return render(request, 'josephscrumy/home.html', context)

# def home(request):
#     to_display = ScrumyGoals.objects.filter(goal_name='Keep Learning Django')
#     output = ', '.join([eachgoal.goal_name for eachgoal in to_display])
#     return HttpResponse(output)


def home(request):
    user = User.objects.all()
    week = GoalStatus.objects.get(status_name="Weekly Goal")
    weeke = week.scrumygoals_set.all()
    daily = GoalStatus.objects.get(status_name="Daily Goal")
    dailyed =daily.scrumygoals_set.all()
    verify = GoalStatus.objects.get(status_name="Verify Goal")
    verifyed=verify.scrumygoals_set.all()
    done = GoalStatus.objects.get(status_name="Done Goal")
    doneed =done.scrumygoals_set.all()
    
    

    # context = {
    #     'user': user,
    #     'done':doneed,
    #     'verify':verifyed,
    #     'daily':dailyed,
    #     'week':weeke
    # }
    context = {
        'user': User.objects.all(),
        'done':GoalStatus.objects.get(status_name="Done Goal").scrumygoals_set.all(),
        'verify':GoalStatus.objects.get(status_name="Verify Goal").scrumygoals_set.all(),
        'daily':GoalStatus.objects.get(status_name="Daily Goal").scrumygoals_set.all(),
        'week':GoalStatus.objects.get(status_name="Weekly Goal").scrumygoals_set.all()
    }
    return render(request, 'josephscrumy/home.html', context)
    


