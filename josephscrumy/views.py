from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from .forms import CreateGoalForm,SignupForm,MovrGoalForm,SwitchGroupForm

from .models import (GoalStatus, ScrumyGoals, ScrumyHistory)
import random
from django.contrib.auth.models import Group
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout,login

# def get_grading_parameters(request):
#     return HttpResponse('Welcome to Django')

def index(request):
    # return HttpResponse('This is a Scrum Application')
    # goal = ScrumyGoals.objects.filter(goal_name='Learn Django')
    # return HttpResponse(goal)
        
    if request.method == 'POST':
        user_form = SignupForm(request.POST)
        if user_form.is_valid():
            user_form.save()
            my_group = Group.objects.get(name = 'Developer')
            get_user_id = User.objects.get(username = user_form.cleaned_data['username'])
            my_group.user_set.add(get_user_id)
            return redirect('/josephscrumy/accounts/login/')
            return HttpResponse("Your account has been created successfully")
        
        else:
            return redirect("josephscrumy:about")

    else:
        user_form = SignupForm()
        return render(request, 'josephscrumy/index.html', context={"user_form": user_form})

#lflllflf

def sucessview(request):
    return render(request, 'josephscrumy/success.html')



@login_required(login_url='/josephscrumy/accounts/login/')
def move_goal(request, goal_id):
    # ScrumyGoals.objects.get(goal_id=goal_id)
    # goal = ScrumyGoals.objects.get(goal_name='Learn Django')
    # return HttpResponse(goal)
    try: 
        obj = ScrumyGoals.objects.get(goal_id=goal_id) 
    except Exception as e: 
        return render(
            request, 
            'josephscrumy/exception.html', 
            {'error': 'A record with that goal id does not exist'}
        )
    else: 
        if request.method == 'POST':
            form = MovrGoalForm(request.POST)
            if form.is_valid():
                forms = form.cleaned_data['goal_status']
               
                Owner = Group.objects.get(name ="Owner")
                Developer = Group.objects.get(name ="Developer")
                qty = Group.objects.get(name ="Quality Assurance")
                ad = Group.objects.get(name ="Admin")
                donegoal = GoalStatus.objects.get(status_name = "Done Goal")
                weeklygoal = GoalStatus.objects.get(status_name = "Weekly Goal")

                if obj.goal_status == forms:
                    return HttpResponse("you cannot move your current goal back to yourself")
                else:
                    if obj.user == request.user:
                        
                        if Developer in request.user.groups.all():
                            if forms == donegoal:
                                return HttpResponse("A member of the 'Developer' group cannot move goals to done goal status")
                            else:
                                obj.goal_status = forms
                                obj.save()
                                return redirect("josephscrumy:home")
                        elif qty in request.user.groups.all():
                            if forms == weeklygoal:
                                return HttpResponse("A member of the 'Quality Assurance' group cannot move goals to weekly goal status")
                            else:
                                obj.goal_status = forms
                                obj.save()
                                return redirect("josephscrumy:home")
                        elif Owner in request.user.groups.all():
                            obj.goal_status = forms
                            obj.save()
                            return redirect("josephscrumy:home")

                    elif qty in request.user.groups.all():
                        if forms == donegoal:
                            obj.goal_status = forms
                            obj.save()
                            return redirect("josephscrumy:home")
                        else:
                            return HttpResponse("The permission is only granted for 'Done Goal'....!")

                    elif ad in request.user.groups.all():
                        obj.goal_status = forms
                        obj.save()
                        return redirect("josephscrumy:home")

                    else:
                        return HttpResponse("You don't have the right to move anybody's goal")
            else:
                return redirect("josephscrumy:add_goal")
        else:
            form = MovrGoalForm()
            return render(request, 'josephscrumy/move.html', context={"form": form})



@login_required(login_url='/josephscrumy/accounts/login/')
def add_goal(request):
    if request.method == 'POST':
        add_main_goal = CreateGoalForm(request.POST)
        if add_main_goal.is_valid():
            if request.user:
                ScrumyGoals.objects.create(
                    user= request.user,
                    goal_name=add_main_goal.cleaned_data['goal_name'],
                    goal_id= random.randint(1000,9999),
                    created_by = 'Louis',
                    moved_by='Louis',
                    owner='Louis',
                    goal_status=GoalStatus.objects.get(status_name='Weekly Goal')
                )
                return redirect("josephscrumy:home")
            else:
                return HttpResponse("A user with the group of 'Developer' can only create goal for himself")
        else:
            return redirect("josephscrumy:add_goal")

    else:
        goal_form = CreateGoalForm()
        return render(request, 'josephscrumy/create.html', context={"user_form": goal_form, "grouped": Group.objects.get(name = "Developer")})


# def home(request):
#     goal_name = ScrumyGoals.objects.get(goal_name='Learn Django')
#     goal_id = int(ScrumyGoals.objects.get(goal_id=1))
#     user = User.objects.get(username='louis')
#     context = {
#         'goal_name': goal_name,
#         'goal_id': goal_id,
#         'user': user,
#     }
#     return render(request, 'josephscrumy/home.html', context)

# def home(request):
#     to_display = ScrumyGoals.objects.filter(goal_name='Keep Learning Django')
#     output = ', '.join([eachgoal.goal_name for eachgoal in to_display])
#     return HttpResponse(output)


def home(request):
    user = User.objects.all()
    week = GoalStatus.objects.get(status_name="Weekly Goal")
    weeke = week.scrumygoals_set.all()
    daily = GoalStatus.objects.get(status_name="Daily Goal")
    dailyed =daily.scrumygoals_set.all()
    verify = GoalStatus.objects.get(status_name="Verify Goal")
    verifyed=verify.scrumygoals_set.all()
    done = GoalStatus.objects.get(status_name="Done Goal")
    doneed =done.scrumygoals_set.all()
    
    

    # context = {
    #     'user': user,
    #     'done':doneed,
    #     'verify':verifyed,
    #     'daily':dailyed,
    #     'week':weeke
    # }
    context = {
        'user': User.objects.all(),
        'done':GoalStatus.objects.get(status_name="Done Goal").scrumygoals_set.all(),
        'verify':GoalStatus.objects.get(status_name="Verify Goal").scrumygoals_set.all(),
        'daily':GoalStatus.objects.get(status_name="Daily Goal").scrumygoals_set.all(),
        'week':GoalStatus.objects.get(status_name="Weekly Goal").scrumygoals_set.all(),
        'dev':Group.objects.get(name = 'Developer')
    }
    return render(request, 'josephscrumy/home.html', context)
    


@login_required(login_url='/josephscrumy/accounts/login/')
def logout_view(request):
    logout(request)
    return redirect('/josephscrumy/accounts/login/')



def groupview(request):
    if request.method == 'POST':
        form = SwitchGroupForm(request.POST)
        if form.is_valid():
            try: 
                g = Group.objects.get(name = form.cleaned_data['name'])
            except Exception as e: 
                return HttpResponse("Sorry, no such group")
            else:
                user_group = User.groups.through.objects.get(user=request.user)
                user_group.group = g
                user_group.save()
                return redirect("josephscrumy:home")
        else:
            return redirect("josephscrumy:group")
    else:
        form = SwitchGroupForm()
        return render(request, 'josephscrumy/group.html', context = {"form": form})