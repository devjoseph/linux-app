from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, User
from django.forms.forms import Form
from .models import ScrumyGoals,GoalStatus

User = get_user_model()
fields = ['first_name', 'last_name', 'email', 'username', 'password']

class SignupForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    confirm_password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'username']
        help_texts = {
            'username': None,
            'email': None,
        }
    
    def clean(self):
        cleaned_data = super(SignupForm, self).clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")

        if password != confirm_password:
            raise forms.ValidationError("password and confirm_password does not match")
        return cleaned_data

    def save(self, commit = True):
        user = super(SignupForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user
        


class CreateGoalForm(forms.ModelForm):
    class Meta:
        model = ScrumyGoals
        fields = ['goal_name',]


class MovrGoalForm(forms.ModelForm):
    class Meta:
        model = ScrumyGoals
        fields = ['goal_status',]



Form_group =( 
    ("--- Choose ---", "--- Choose ---"),
    ("Developer", "Developer"), 
    ("Owner", "Owner"), 
    ("Admin", "Admin"), 
    ("Quality Assurance", "Quality Assurance")
)



class SwitchGroupForm(forms.Form):
    name = forms.ChoiceField(choices = Form_group)
