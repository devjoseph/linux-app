from django.urls import path,include
from . import views

app_name = 'josephscrumy' 

urlpatterns = [
    path('', views.index, name="about"),
    path('movegoal/<int:goal_id>/', views.move_goal, name="movegoal"),
    path('addgoal/', views.add_goal, name="add_goal"),
    path('home/', views.home, name="home"),
    path('accounts/',include('django.contrib.auth.urls')),
    path('success', views.sucessview, name="sucess"),
    path('logout/', views.logout_view, name="logout"),
    path('group/', views.groupview, name="group")
    
]
